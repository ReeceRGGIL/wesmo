<?php
  require_once('logged-in.inc.php');
  if ($_SESSION['role'] != 'manager') {
    header('location: analyse.php');
    exit();
  }
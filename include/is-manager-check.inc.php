<?php
  require_once('logged-in-check.inc.php');
  if ($_SESSION['role'] != 'manager') {
    exit();
  }
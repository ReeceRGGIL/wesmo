<?php 
  if (!isset($_SESSION['email']) || !isset($_SESSION['fname']) || !isset($_SESSION['lname']) || !isset($_SESSION['role'])) {
    session_destroy();
    exit();
  } else if ($_SESSION['role'] == 'unapproved-member' || $_SESSION['role'] == 'unapproved-driver' || $_SESSION['role'] == 'unapproved-manager') {
    exit();
  }
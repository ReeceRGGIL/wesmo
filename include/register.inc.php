<?php 
  if($_SERVER['REQUEST_METHOD'] == 'POST') {
    include_once('db.inc.php');
    
    $email = mysqli_real_escape_string($link, $_POST['email']);
    $pword = mysqli_real_escape_string($link, $_POST['pword']);
    $pwordConfirm = mysqli_real_escape_string($link, $_POST['pword-confirm']);
    $fname = mysqli_real_escape_string($link, $_POST['fname']);
    $lname = mysqli_real_escape_string($link, $_POST['lname']);
    $role = mysqli_real_escape_string($link, $_POST['role']);
    $height = mysqli_real_escape_string($link, $_POST['height']);
    $weight = mysqli_real_escape_string($link, $_POST['weight']);
    
    /* Check that main fields are not empty */
    if (empty($email) || empty($pword) || empty($pwordConfirm)|| empty($fname) || empty($lname) || empty($role)) {
      header("Location: register.php?register=empty-field");
      exit();
    } else {
      /* Check that driver fields are not empty */
      if ($role == 'driver' && (!is_numeric($height) || !is_numeric($weight))) {
        header("Location: register.php?register=driver-fields-empty");
        exit();
      } else {
        /* Check that Name is valid */
        if (!preg_match("/^[a-zA-Z]*$/", $fname) || !preg_match("/^[a-zA-Z]*$/", $lname)) {
          header("Location: register.php?register=invalid-name");
          exit();
        } else {
          /* Check that Email is valid */
          if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            header("Location: register.php?register=invalid-email");
            exit();
          } else {
            /* Check that email isnt taken */
            $sql = "SELECT * FROM members WHERE email='$email'";
            $result = mysqli_query($link, $sql);
            $resultCheck = mysqli_num_rows($result);
            if ($resultCheck > 0) {
              header("Location: register.php?register=email-taken");
              exit();
            } else {
              /* Check that passwords match */
              if ($pword != $pwordConfirm) {
                header("Location: register.php?register=password-mismatch");
                exit();
              } else {
                /* Checks complete */
                if ($role == 'member') {
                  $role = 'unapproved-member';
                } elseif ($role == 'driver') {
                  $role = 'unapproved-driver';
                } elseif ($role =='manager') {
                  $role = 'unapproved-manager';
                }
                
                /* Inserting Member */
                $hashedPword = password_hash($pword, PASSWORD_DEFAULT);
                $sql = "INSERT INTO members(email,fname,lname,role,pword) VALUES ('$email','$fname','$lname','$role','$hashedPword')";
                mysqli_query($link, $sql);
                
                /* Inserting Driver */
                if ($role == 'driver' || $role == 'unapproved-driver') {
                  $sql = "INSERT INTO Drivers(email,height,weight) VALUES ('$email','$height','$weight')";
                  mysqli_query($link, $sql);
                }
                
                header("Location: unapproved.php");
                exit();
              }
            }
          }
        }
      }
    }
  }
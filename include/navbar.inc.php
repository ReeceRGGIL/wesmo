<nav class="nav-bar">
  <a href="analyse.php">
    <div class="nav-div">Analyse</div>
  </a>
  <a href="data.php">
    <div class="nav-div" <?php if ($_SESSION['role'] != 'manager') {echo "hidden='true'";}?> >Data</div>
  </a>
  <a href="members.php">
    <div class="nav-div" <?php if ($_SESSION['role'] != 'manager') {echo "hidden='true'";}?> >Members</div>
  </a>
  <a href="php/logout.php">
    <div class="nav-div nav-div-right">Log out</div>
  </a>
  <div class="nav-div nav-div-right">
    <div class="vertical-content">
      <img class="nav-logo" src="img/wesmo-logo-black.png" alt="WESMO Logo">
    </div>
  </div>
</nav>
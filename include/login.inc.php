<?php
  require_once('db.inc.php');

  if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['email']) && isset($_POST['password']))
  {
    $email = mysqli_real_escape_string($link, $_POST['email']);
    $password = mysqli_real_escape_string($link, $_POST['password']);
    
    if (empty($email) || empty($password)) {
      header("Location: login.php?login=empty-error");
      exit();
    } else {
      $sql = "SELECT * FROM `members` WHERE email = '$email'";
      $result = mysqli_query($link, $sql);
      $resultCheck = mysqli_num_rows($result);
      if ($resultCheck < 1) {
        header("Location: login.php?login=result-error");
        exit();
      } else {
        if ($row = mysqli_fetch_assoc($result)) {
          $passwordCheck = password_verify($password, $row['pword']);
          if ($passwordCheck == false) {
            header("Location: login.php?login=password-error");
            exit();
          } else {
            $_SESSION['email'] = $row['email'];
            $_SESSION['fname'] = $row['fname'];
            $_SESSION['lname'] = $row['lname'];
            $_SESSION['role'] = $row['role'];
            header("Location: analyse.php");
            exit();
          }
        } else {
          header("Location: login.php?login=fetch-error");
          exit();
        }
      }
    }
  }
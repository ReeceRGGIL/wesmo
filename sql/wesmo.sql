create table Members(
    email varchar(254) NOT NULL,
    fname varchar(50) NOT NULL,
    lname varchar(50) NOT NULL,
    role varchar(50) NOT NULL,
    pword varchar(255) NOT NULL,
    PRIMARY KEY (email)
);

create table Drivers(
    email varchar(254) NOT NULL,
    height integer,
    weight integer,
    PRIMARY KEY (email),
    FOREIGN KEY driver_key(email)
    REFERENCES Members(email)
);

create table Runs(
    id integer NOT NULL AUTO_INCREMENT,
    driver varchar(254) NOT NULL,
    run_name varchar(50) NOT NULL,
    venue varchar(50),
    run_date date NOT NULL,
    duration integer NOT NULL,
    conditions varchar(50),
    track_temp decimal,
    sample_rate integer NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY driver_performed(driver)
    REFERENCES Drivers(email)
);

create table Sensor_Readings(
    Time_Stamp double NOT NULL,
    EngineRPM double,
    Throttle_Pos double,
    Eng_Oil_Pres double,
    GForce_Lat double,
    GForce_Long double,
    GForce_Vert double,
    Gear double,
    SuspPos_FL double,
    SuspPos_FR double,
    SuspPos_RL double,
    SuspPos_RR double,
    GPSLatitude double,
    GPSLongitude double,
    GPSTime double,
    GPSSpeed double,
    Engine_Temp double,
    Lap_Number double,
    Lap_Time double,
    Fuel_Inj_Duty double,
    Fuel_Actual_PW double,
    Ign_Advance double,
    Air_Temp_Inlet double,
    Bat_Volts_SDL double,
    Lap_Distance double,
    GPS_Heading double,
    Lap_GainLoss_Final double,
    Lap_GainLoss_Running double,
    Lambda1 double,
    Wheel_Speed_FL double,
    Wheel_Speed_FR double,
    Wheel_Speed_RL double,
    Wheel_Speed_RR double,
    Steered_Angle double,
    Reference_Lap_Time double,
    Manifold_Pres double,
    Odometer double,
    Fuel_Pres double,
    Brake_Status double,
    RunID integer NOT NULL,                
    PRIMARY KEY (RunID, Time_Stamp),
    FOREIGN KEY run_contains(RunID)
    REFERENCES Runs(id)
);


/* Create Data */

INSERT INTO Members(email,fname,lname,role,pword)
VALUES
('fk@hotmail.com','flavio','klages','member','$2y$10$1ttWCzge2hrGolBlvEn0meiMTgI.WcWj0iSUHHZ6OmPyzcgqSozQu'), /* All passwords are 'password' */
('oakley@mail.com','Oakley','Drewet','manager','$2y$10$c7v8iQO./KIGEY.W50Zkf.VGpque2kZRtY3oW/gKdIddLJbOXs/Su'),
('david@mail.com','David','Collins','driver','$2y$10$bLTPRRxFS7YFgqy6YYh7s.jJ4xyh5udpkYngOvnVSqWOQvajrWgra'),
('jw@gmail.com','John','Walker','driver','$2y$10$ZH6ZA6OG9.9r334YmYvTiu7PXhs1J1gOhqLQi.yAKYZe2LxEmQoIG'),
('rs@mail.com','Robert','shaw','manager','$2y$10$pvveGEo5ccUBNUJ/sKwObehtYxN0sHOXxc2KsvI/tnn2SkQlys1ii'),
('kl@mail.com','Keith','Lamas','member','$2y$10$SQmqu2ClTGnA6UIEl.qIRO8wHs5GUjO5PMlgheUxKwdIXLMVSXgAi'),
('jh@mail.com','Jason','Huds','member','$2y$10$MpizsInH102ChI/P8sWvOue.rRq8KqLDFvMXBsOXA0vSPIvOymyGK'),
('pw@mail.com','Phillip','Watkins','member','$2y$10$NeYvcRQzoj00HTWCM9KpoezGy0QVqzBv7N1d1uzkYEppXEkg9PEQ.'),
('cp@mail.com','Clinton','Parker','member','$2y$10$Dyby/MvvkOFuFLfTbB8zmu6u9f46OPTB.2T.jNW0D/t/uyZlxdNFG'),
('cm@mail.com','Charles ','Martin','member','$2y$10$nQp2Dzmhu/yDJ6.ah37R4uUNDmeb96tWroHNAnpVP7oc0LesD.zqe'),
('li@mail.com','Louis','Ingle','member','$2y$10$JKwPciA/5pmYSnKeMRlJNeoTFboMGP0cPumfoQyvore5PZUU4B0qO'),
('nf@mail.com','Natasha','Feldt','member','$2y$10$4KHSc6huYleaXDacOUMzhu0RtWTP.OI7WKm/8d1VvDzO9doHJE5cm'),
('cn@mail.com','Cameron ','Newcomb','member','$2y$10$bchUKVRU7m8ZikDLMtFOnOJiJgiHKd0/f1MKHu/gnWWHVu30ytovC'),
('as@mail.com','Alana ','Salter','member','$2y$10$ByeSO5mWHQLsKtpTcRcGsuMQxB0pfSdYPhUbev8bc3vclcfChhKru'),
('fy@mail.com','Flynn ','Youl','member','$2y$10$9PuMT32I53SXH3sMxX2vdOgJfKZsYvsL59VXpvCr1Fg0RFta4yO32'),
('mh@mail.com','Mason ','Heyer','member','$2y$10$zN2LuVkJ3ncmrodhc2qEVO/OQ2Es2Eg.NOO6WNVgwLmxNsOP4zK12'),
('jm@mail.com','Jessica ','McKenna','member','$2y$10$rEqlxWObfDVjBz6esc5cOOZ16UTZemAreeDVdru41sQDTwBesPWAa'),
('lm@mail.com','Lola ','Mead','member','$2y$10$K1uC70m.s5j9e/xczeM/m.YveMvB157LWhLpCQ3fvHHgm/ddB17UC'),
('dr@mail.com','Darcy ','Ranken','member','$2y$10$4O6AWRC1hBJfWprxFWA20.1HZ5S2TuRZel7aXcUFBUbNtcRzQGBa2'),
('sm@mail.com','Stephanie ','Marston','member','$2y$10$O6arh01kHmVfzvufWOjTQubXNWhvdZ126cNJ16FVtmeyPjYmfoJ.i'),
('jk@mail.com','Justin ','Klein','member','$2y$10$zFheTR1HI2ZxoAgKRIzc.unMvHhnXb.wcUpBdLIOR7kfJO78DG5U2'),
('lk@mail.com','Lilly ','Kirtley','member','$2y$10$8/9s6cFC6g7sW0f10xJw3.1XVVzti/.LjYC0bl0pGKjEQ57IWeK4i'),
('rr@mail.com','Ryder ','Rigby','member','$2y$10$ksVvcTISxANwUnOZSSycE.aI9yc80otMchmpbZnxQLbQf0q0119Ai'),
('ls@mail.com','Liam ','Schreiber','member','$2y$10$A76fQz3h33hfJ9YDQYfBhukdVXDGifyzFsmyiuwvtIg8YWuorchGS'),
('justinm@mail.com','Justin ','MacNeil','member','$2y$10$zfxKDWqS5TT5CDA5L0dvveVJfAT2O9eDZsTWS.GwtrdbuUYH/YHFK'),
('lukes@mail.com','Luke ','Spencer','member','$2y$10$MWKYN2TeOMfSC3.WRyPu0eMvOHHc6wXm2zs19jW1A3.AdvDhOrlNK'),
('js@mail.com','Joseph','Williams','driver','$2y$10$NPr9v3dWymzxTlVKkCDFeuQBcRQTMf7ccibhHKOcTVDXi8FVbDTOa'),
('ds@mail.com','David','Sparkman','driver','$2y$10$WiDIwrw5pu0jUGr4R8lNHuR6IN3MihsUVKk5bZFeF15l4jFwEJJ.G'),
('mm@mail.com','Morris','monroe','driver','$2y$10$Ohl0mpfKhzLAtiHsfRhUpOmMJPMgjFDeiol1DBaoutaHotULrh2aO');

INSERT INTO Drivers(email,height,weight)
VALUES
('david@mail.com',175,72),
('jw@gmail.com',178,78),
('js@mail.com',169,82),
('ds@mail.com',178,105),
('mm@mail.com',167,92);

INSERT INTO Runs(driver,run_name,venue,run_date,duration,conditions,track_temp,sample_rate)
VALUES
('david@mail.com','Garage_GOKart 1','Go Kart Track','2017-09-26','453','raining', 17.56,20),
('jw@gmail.com','Garage_GOKart 2','Go Kart Track','2017-09-18','638','cloudy', 19.89,200),
('js@mail.com','Garage_GOKart 3','Go Kart Track','2017-09-25','313','clear', 19.89,200);

/* Replace H:\... with directory containing files */
LOAD DATA LOCAL INFILE 'H:\Garage_GoKart_SuspensionTrack1.csv'
INTO TABLE Sensor_Readings
FIELDS TERMINATED BY ','
IGNORE 18 LINES
SET RunID = 1;

LOAD DATA LOCAL INFILE 'H:\Garage_GoKart_SuspensionTrack2.csv'
INTO TABLE Sensor_Readings
FIELDS TERMINATED BY ','
IGNORE 18 LINES
SET RunID = 2;

LOAD DATA LOCAL INFILE 'H:\Garage_GoKart_SuspensionTrack3.csv'
INTO TABLE Sensor_Readings
FIELDS TERMINATED BY ','
IGNORE 18 LINES
SET RunID = 3;

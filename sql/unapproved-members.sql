INSERT INTO Members(email,fname,lname,role,pword)
VALUES
('kal@mail.com','Katherine','Lee','unapproved-member','$2y$10$1ttWCzge2hrGolBlvEn0meiMTgI.WcWj0iSUHHZ6OmPyzcgqSozQu'), /* All passwords are 'password' */
('shr@mail.com','Shonda','Ramsey','unapproved-manager','$2y$10$c7v8iQO./KIGEY.W50Zkf.VGpque2kZRtY3oW/gKdIddLJbOXs/Su'),
('shs@mail.com','Shannon','Smith','unapproved-driver','$2y$10$bLTPRRxFS7YFgqy6YYh7s.jJ4xyh5udpkYngOvnVSqWOQvajrWgra'),
('aub@mail.com','Austin','Bynum','unapproved-driver','$2y$10$ZH6ZA6OG9.9r334YmYvTiu7PXhs1J1gOhqLQi.yAKYZe2LxEmQoIG'),
('ang@mail.com','Andrea','Gallucci','unapproved-manager','$2y$10$pvveGEo5ccUBNUJ/sKwObehtYxN0sHOXxc2KsvI/tnn2SkQlys1ii');

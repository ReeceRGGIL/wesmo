<?php 
  session_start();
  require("/include/logged-in.inc.php");
  require("/include/is-manager.inc.php");
  require_once 'include/db.inc.php';
?>
<!DOCTYPE html>
<head>
  <title>Members - Wesmo Viewer</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="icon" href="img/wesmo-icon.ico">
  <script type="text/javascript" src="js/members.js" async></script>
</head>
<body>
  
  <?php include('include/navbar.inc.php') ?>
  
  <!--<nav class="nav-bar">
    <a href="analyse.php">
      <div class="nav-div">Analyse</div>
    </a>
    <a href="data.php">
      <div class="nav-div">Data</div>
    </a>
    <a href="members.php">
      <div class="nav-div">Members</div>
    </a>
    <div class="nav-div">
      <div class="vertical-content">
        <img class="nav-logo" src="img/wesmo-logo-black.png" alt="WESMO Logo">
      </div>
    </div>
    <a href="php/logout.php">
      <div class="nav-div">Log out</div>
    </a>
  </nav>-->
  
  <div class="content">
    <div class="table-container">
      <table class="members-table">
        <thead>
          <tr>
            <th>Email</th>
            <th>Name</th>
            <th>Role</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $sql = "SELECT email,fname,lname,role FROM members WHERE role IN ('unapproved-member','unapproved-driver','unapproved-manager')";
            $results = mysqli_query($link, $sql);
            while ($row = mysqli_fetch_array($results)) : 
          ?>
          <tr>
            <td><?php echo $row['email']; ?></td>
            <td><?php echo ($row['fname'] . " " . $row['lname']); ?></td>
            <td><?php echo $row['role']; ?></td>
            <td>
              <button type="button" name="accept" value="<?php echo $row['email']; ?>">Accept</button>
              <button type="button" name="decline" value="<?php echo $row['email']; ?>">Decline</button>
            </td>
          </tr>
        <?php endwhile ?>
        <?php 
          $sql = "SELECT email,fname,lname,role FROM members WHERE role IN ('member','driver','manager')";
          $results = mysqli_query($link, $sql);
          while ($row = mysqli_fetch_array($results)) : 
        ?>
        <tr>
          <td><?php echo $row['email']; ?></td>
          <td><?php echo ($row['fname'] . " " . $row['lname']); ?></td>
          <td>
            <select class="role-select">
              <option value="member" <?php if($row['role'] == 'member'){ echo 'selected';}?> >Member</option>
              <option value="manager" <?php if($row['role'] == 'manager'){ echo 'selected';}?> >Manager</option>
              <option value="driver" <?php if($row['role'] == 'driver'){ echo 'selected';}?> >Driver</option>
            <?php echo $row['role']; ?>
          </td>
          <td><input type="button" name="remove" value="Remove"></td>
        </tr>
      <?php endwhile ?>
        </tbody>
      </table>
    </div>
  </div>
</body>

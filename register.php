<?php 
  session_start();
  require('/include/register.inc.php');
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Register - Wesmo Viewer</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" href="img/wesmo-icon.ico">
    <script type="text/javascript" src="js/register.js" async></script>
  </head>
  <body>
    <div class="vertical-content">
      <div class="register">
        <img src="img/wesmo-logo-black.png" alt="wesmo logo">
        <h1>Register</h1>
        <form action="" method="post">
          <label class="block-label" for="name">First Name</label>
          <input class="block-input" type="text" name="fname" placeholder="John" required>
          <label class="block-label" for="name">Last Name</label>
          <input class="block-input" type="text" name="lname" placeholder="Smith" required>
          <label class="block-label" for="email" >Email</label>
          <input class="block-input" type="email" name="email" placeholder="name@domain.com" required>
          <label class="block-label" for="pword" >Password</label>
          <input class="block-input" type="password" name="pword" placeholder="Password" required>
          <label class="block-label" for="pword-confirm" >Confirm Password</label>
          <input class="block-input" type="password" name="pword-confirm" placeholder="Retype Password" required>
          <label class="block-label" for="role" >Role</label>
          <select class="block-select" name="role" id="role-select">
            <option value="member" selected>Team Member</option>
            <option value="driver">Driver</option>
            <<option value="manager">Team Manager</option>
          </select>

          <div id="driver-info" hidden="true">
            <label class="block-label" for="height">Height (cm)</label>
            <input class="block-input" type="text" name="height" placeholder="180">
            <label class="block-label" for="weight">Weight (kg)</label>
            <input class="block-input" type="text" name="weight" placeholder="90">
          </div>
          
          <input class="block-button" type="submit" value="Register">
        </form>
      </div>
    </div>
  </body>
</html>

<?php
  session_start();
  require '/include/db.inc.php';
  require '/include/login.inc.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Login - Wesmo Viewer</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" href="img/wesmo-icon.ico">
  </head>
  <body>
    <div class="vertical-content">
      <div class="login">
        <img src="img/wesmo-logo-black.png" alt="wesmo logo">
        <h1>Login</h1>
        <form action="" method="post">
          <label class="block-label" for="email" >Email</label>
          <input class="block-input" type="email" name="email" placeholder="name@domain.com" required />
          <label class="block-label" for="password">Password</label>
          <input class="block-input" type="password" name="password" placeholder="Password" required />
          <input type="submit" value="Submit" class="block-button"/>
        </form>
        <a href="register.php" id="register-text">Register?</a>
      </div>
    </div>
  </body>
</html>

var numData = 0;    //Number of runs currently selected
var chart;          //Variable to hold the chart attributes

function createGraph()
{
    getRuns();
    
    nv.addGraph(function()
    {

      chart = nv.models.lineWithFocusChart();

      chart.xAxis
            .axisLabel('Time (s)')
          .tickFormat(d3.format(',f'));

      chart.yAxis
            .axisLabel('Engine RPM')
          .tickFormat(d3.format(',.2f'));

      chart.y2Axis
          .tickFormat(d3.format(',.2f'));

      d3.select('#chart svg')
          .datum(testData(0))
          .transition().duration(500)
          .call(chart);

      nv.utils.windowResize(chart.update);

      return chart;
    });
}

function updateGraph(numLines)
{    
    d3.select('#chart svg')
          .datum(testData(numLines))
          .transition().duration(500)
          .call(chart);

    nv.utils.windowResize(chart.update);
}


/*
 * Generate test data
 */
function testData(numLines) {
  return stream_layers(numLines,10+Math.random()*200,.1).map(function(data, i) {
    return {
      key: 'Driver ' + i,
      values: data
    };
  });
}

/* Inspired by Lee Byron's test data generator. */
function stream_layers(n, m, o) {
  if (arguments.length < 3) o = 0;
  function bump(a) {
    var x = 1 / (.1 + Math.random()),
        y = 2 * Math.random() - .5,
        z = 10 / (.1 + Math.random());
    for (var i = 0; i < m; i++) {
      var w = (i / m - y) * z;
      a[i] += x * Math.exp(-w * w);
    }
  }
  return d3.range(n).map(function() {
      var a = [], i;
      for (i = 0; i < m; i++) a[i] = o + o * Math.random();
      for (i = 0; i < 5; i++) bump(a);
      return a.map(stream_index);
    });
}

/* Another layer generator using gamma distributions. */
function stream_waves(n, m) {
  return d3.range(n).map(function(i) {
    return d3.range(m).map(function(j) {
        var x = 20 * j / m - i / 3;
        return 2 * x * Math.exp(-.5 * x);
      }).map(stream_index);
    });
}

function stream_index(d, i) {
  return {x: i, y: Math.max(0, d)};
}


function getRuns()
{
    
    
    //Construct url for request
    var url = "./php/getRunSelection.php";
    //Send request with the callback function to add the line to the widget
    _ajaxRequest("GET", url, true, "", addRunsToList);
}

function addRunsToList(response)
{
    //Parse the response from JSON format
    var data = JSON.parse(response);
    
    //Could not get the specified town
    if(data.length == 0)
    {
        alert("No runs available.");
        return;
    }
    
    var list = document.getElementById("available");
    
    for(i = 0; i < data.length; i++)
    {
        var item = document.createElement("option");
        item.value = data[i].id;
        item.innerHTML = data[i].run_name + " -- " + data[i].fname + " " + data[i].lname;
        list.appendChild(item);
    }
    
    getAttributes();
}


function addSelectedRuns()
{
    var available = document.getElementById("available");
    var selected = document.getElementById("selected");
    var index = available.selectedIndex;
    
    for(var i = 0; i < available.options.length; i++)
    {
        if(available.options[i].selected)
        {
            selected.appendChild(available.options[i]);
            
            numData++;
            //alert(numData);
            updateGraph(numData);
        }
    }
    
    addStat(index);
    
    available.selectedIndex = -1;
}

function removeSelectedRuns()
{
    var available = document.getElementById("available");
    var selected = document.getElementById("selected");
    var index = selected.selectedIndex;
    
    for(var i = 0; i < selected.options.length; i++)
    {
        if(selected.options[i].selected)
        {
            available.appendChild(selected.options[i]);
            
            numData--;
            //alert(numData);
            updateGraph(numData);
        }
    }
    
    removeStat(index);
    
    available.selectedIndex = -1;
}


function getAttributes()
{
    var x = document.getElementById("xaxis");
    var y = document.getElementById("yaxis");
    
    var attributes = [
        "Time",
        "EngineRPM",
        "Throttle_Pos",
        "Eng_Oil_Pres",
        "GForce_Lat",
        "GForce_Long",
        "GForce_Vert",
        "Gear",
        "SuspPos_FL",
        "SuspPos_FR",
        "SuspPos_RL",
        "SuspPos_RR",
        "GPSLatitude",
        "GPSLongitude",
        "GPSTime",
        "GPSSpeed",
        "Engine_Temp",
        "Lap_Number",
        "Lap_Time",
        "Fuel_Inj_Duty",
        "Fuel_Actual_PW",
        "Ign_Advance",
        "Air_Temp_Inlet",
        "Bat_Volts_SDL",
        "Lap_Distance",
        "GPS_Heading",
        "Lap_GainLoss_Final",
        "Lap_GainLoss_Running",
        "Lambda1",
        "Wheel_Speed_FL",
        "Wheel_Speed_FR",
        "Wheel_Speed_RL",
        "Wheel_Speed_RR",
        "Steered_Angle",
        "Reference_Lap_Time",
        "Manifold_Pres",
        "Odometer",
        "Fuel_Pres",
        "Brake_Status",
    ];
    
    //Puts all attributes into x-axis selection
    for(var i = 0; i < attributes.length; i++)
    {
        var opt = attributes[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        
        x.appendChild(el);
    }
    
    //Makes time the default value
    for(var i, j = 0; i = x.options[j]; j++) {
        if(i.value == "Time") {
            x.selectedIndex = j;
            break;
        }
    }
    
    
    //Puts all attributes into y-axis selection
    for(var j = 0; j < attributes.length; j++)
    {
        var opt = attributes[j];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
      
        y.appendChild(el);
    }
    
    //Makes rpm the default value
    for(var i, j = 0; i = y.options[j]; j++) {
        if(i.value == "EngineRPM") {
            y.selectedIndex = j;
            break;
        }
    }
}


function addStat(index)
{
    if(index > -1)
    {
        //Delete the placeholder if first one
        if(document.getElementById("selected").options.length == 1)
        {
            var headElement = document.getElementById("header").lastChild;
            headElement.parentNode.removeChild(headElement);

            var rows = document.getElementById("data").children;
            for (var i = 0; i < rows.length; i++)
            {
                var dataElement = rows[i].lastChild
                dataElement.parentNode.removeChild(dataElement);
            }
        }

        var header = document.getElementById("header");
        var name = document.createElement("th");
        name.innerHTML = "Run " + document.getElementById("selected").options.length;
        header.appendChild(name);

        var rows = document.getElementById("data").children;

        for (var i = 0; i < rows.length; i++)
        {
            var datum = document.createElement("td");
            datum.innerHTML = (Math.random()*49+1).toFixed(1);
            rows[i].appendChild(datum);
        }
    }
}

function removeStat(index)
{
    if(index > -1)
    {
        var headElement = document.getElementById("header").lastChild;
        headElement.parentNode.removeChild(headElement);

        var rows = document.getElementById("data").children;
        for (var i = 0; i < rows.length; i++)
        {
            var dataElement = rows[i].lastChild
            dataElement.parentNode.removeChild(dataElement);
        }
    }
    
    //Add the placeholder if first one
    if(document.getElementById("selected").options.length == 0)
    {
        var header = document.getElementById("header");
        var name = document.createElement("th");
        name.innerHTML = "Runs";
        header.appendChild(name);

        var rows = document.getElementById("data").children;

        for (var i = 0; i < rows.length; i++)
        {
            var datum = document.createElement("td");
            rows[i].appendChild(datum);
        }
    }
}


function selectYAxis(axisName)
{
    chart.yAxis.axisLabel(axisName);
    updateGraph(numData);
}

function selectXAxis(axisName)
{
    chart.xAxis.axisLabel(axisName);
    updateGraph(numData);
}


/*
 * Generic AJAX method to make a request
 */
var _ajaxRequest = function(method, url, async, data, callback)
{
    var request = new XMLHttpRequest();
    request.open(method, url, async);

    if(method == "POST")
    {
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    }

    request.onreadystatechange = function()
    {
        if(request.readyState == 4)
        {
            if(request.status == 200)
            {
                var response = request.responseText;
                callback(response);
            } 
            else 
            {
                //do nothing
            }
        }
    }

    request.send(data);
}


function getData()
{
    //Construct url for request
    var url = "./php/run.php";
    //Send request with the callback function to add the line to the widget
    _ajaxRequest("GET", url, true, "", displayRun);
}

function displayRun(response)
{
    alert(response);
    //Parse the response from JSON format
    var data = response;
    
    if(data.length == 0)
    {
        alert("This run is not available");
        return;
    }

    return data;
}
function createGraph()
{
    getRuns();

    nv.addGraph(function()
    {

      var chart = nv.models.lineWithFocusChart();

      chart.xAxis
            .axisLabel('Time (s)')
          .tickFormat(d3.format(',.2f'));

      chart.yAxis
            .axisLabel('Engine RPM')
          .tickFormat(d3.format(',f'));

      chart.y2Axis
          .tickFormat(d3.format(',.2f'));

        var myData = displayRun();

      d3.select('#chart svg')
          .datum(myData)
          .transition().duration(500)
          .call(chart);

      nv.utils.windowResize(chart.update);

      return chart;
    });
}

/*
 * Generic AJAX method to make a request
 */
var _ajaxRequest = function(method, url, async, data, callback)
{
    var request = new XMLHttpRequest();
    request.open(method, url, async);

    if(method == "POST")
    {
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    }

    request.onreadystatechange = function()
    {
        if(request.readyState == 4)
        {
            if(request.status == 200)
            {
                var response = request.responseText;
                callback(response);
            }
            else
            {
                //do nothing
            }
        }
    }

    request.send(data);
}


function getData()
{
    //Construct url for request
    var url = "php/run.php";
    //Send request with the callback function to add the line to the widget
    _ajaxRequest("GET", url, true, "", displayRun);
}

function displayRun()
{
    //Construct url for request
    var url = "php/run.php";
    var response = "";

    var request = new XMLHttpRequest();
    request.open("GET", url, true);

    request.send();

    request.onreadystatechange = function()
    {
        if(request.readyState == 4)
        {
            if(request.status == 200)
            {
                response = request.responseText;
            }
            else
            {
                //do nothing
            }
        }
    }

    alert(request.responseText);
    //Parse the response from JSON format
    var data = JSON.parse(response);

    //alert(data);

    if(data.length == 0)
    {
        alert("This run is not available");
        return;
    }

    //alert(data.length);
    //alert(data[53].Time_Stamp);
    //alert(data[53].EngineRPM);

    var points = [];

    for(var i = 0; i < data.length; i++)
    {
        //alert(data[i].Time_Stamp);
        points.push({x: data[i].Time_Stamp, y: data[i].EngineRPM});
    }

    return [
        {
            values: points,
            key: 'Driver 1'
        }
    ];
}


/*
 * Generate test data
 */
function testData() {
  return stream_layers(3,10+Math.random()*200,.1).map(function(data, i) {
    return {
      key: 'Driver ' + i,
      values: data
    };
  });
}

/* Inspired by Lee Byron's test data generator. */
function stream_layers(n, m, o) {
  if (arguments.length < 3) o = 0;
  function bump(a) {
    var x = 1 / (.1 + Math.random()),
        y = 2 * Math.random() - .5,
        z = 10 / (.1 + Math.random());
    for (var i = 0; i < m; i++) {
      var w = (i / m - y) * z;
      a[i] += x * Math.exp(-w * w);
    }
  }
  return d3.range(n).map(function() {
      var a = [], i;
      for (i = 0; i < m; i++) a[i] = o + o * Math.random();
      for (i = 0; i < 5; i++) bump(a);
      return a.map(stream_index);
    });
}

/* Another layer generator using gamma distributions. */
function stream_waves(n, m) {
  return d3.range(n).map(function(i) {
    return d3.range(m).map(function(j) {
        var x = 20 * j / m - i / 3;
        return 2 * x * Math.exp(-.5 * x);
      }).map(stream_index);
    });
}

function stream_index(d, i) {
  return {x: i, y: Math.max(0, d)};
}



function getRuns()
{
    //Construct url for request
    var url = "php/getRunSelection.php";
    //Send request with the callback function to add the line to the widget
    _ajaxRequest("GET", url, true, "", addRunsToList);
}

function addRunsToList(response)
{
    //Parse the response from JSON format
    var data = JSON.parse(response);

    //Could not get the specified town
    if(data.length == 0)
    {
        alert("No runs available.");
        return;
    }

    var list = document.getElementById("available");

    for(i = 0; i < data.length; i++)
    {
        var item = document.createElement("option");
        item.value = data[i].id;
        item.innerHTML = data[i].run_name + " -- " + data[i].fname + " " + data[i].lname;
        list.appendChild(item);
    }
}


function addSelectedRuns()
{
    var available = document.getElementById("available");
    var selected = document.getElementById("selected");

    for(var i = 0; i < available.options.length; i++)
    {
        if(available.options[i].selected)
        {
            selected.appendChild(available.options[i]);
        }
    }
}

function removeSelectedRuns()
{
    var available = document.getElementById("available");
    var selected = document.getElementById("selected");

    for(var i = 0; i < selected.options.length; i++)
    {
        if(selected.options[i].selected)
        {
            available.appendChild(selected.options[i]);
        }
    }

    available.selectedIndex = -1;
}

var removeSelectedButton = document.getElementById('remove-selected');
removeSelectedButton.addEventListener("click", removeSelected);

// Event listerners for all role selects
var roleSelects = document.getElementsByClassName("role-select");
var updateRole = function() {
  console.log("Function: UpdateRole");
  var email = this.parentElement.parentElement.firstElementChild.innerHTML;
  var role = this.options[this.selectedIndex].text.toLowerCase();
  console.log(email);
  var xhttp = new XMLHttpRequest();
  xhttp.open("GET", "php/update-role.php?email=" + email + "&role=" + role, true);
  xhttp.send();
};
for (var i = 0; i < roleSelects.length; i++) {
  roleSelects[i].addEventListener('change', updateRole, false);
}

function saveRoles(){
  console.log("Function: saveRoles");
  //TODO:...
};

function removeSelected() {
  console.log("Function: removeSelected");
  //TODO:...
};

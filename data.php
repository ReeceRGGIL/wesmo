<?php 
  session_start();
  require("/include/logged-in.inc.php");
  require("/include/is-manager.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
  <title>Data - Wesmo Viewer</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="css/style.css">
   <link rel="stylesheet" type="text/css" href="css/datastyle.css">
  <link rel="icon" href="img/wesmo-icon.ico">
</head>
<body>
  <nav class="nav-bar">
    <a href="analyse.php">
      <div class="nav-div">Analyse</div>
    </a>
    <a href="data.php">
      <div class="nav-div">Data</div>
    </a>
    <a href="members.php">
      <div class="nav-div">Members</div>
    </a>
    <div class="nav-div">
      <div class="vertical-content">
        <img class="nav-logo" src="img/wesmo-logo-black.png" alt="WESMO Logo">
      </div>
    </div>
    <a href="logout.php">
      <div class="nav-div">Log out</div>
    </a>
  </nav>

   <section class="container">
        <div class="upload">
        <h1>UPLOAD</h1>
        <form action="" method="post">
        <input class="block-input" type="file" accept=".csv"/>
          <label class="block-label" for="name" >Name</label>
          <input class="block-input" type="text" name="name" placeholder="Run Name" required />
          <label class="block-label" for="date">Date</label>
          <input class="block-input" type="date" name="date" placeholder="Run Date" required />
          <label class="block-label" for="location">Location</label>
          <input class="block-input" type="text" name="location" placeholder="Run Loacation" required />
          <label class="block-label" for="time">Time</label>
          <input class="block-input" type="text" name="time" placeholder="Run Time" required />
          <label class="block-label" for="driver">Driver</label>
          <select class="block-input" >
          <option value="1">Driver 1</option>
          <option value="2">Driver 2</option>
          <option value="3">Driver 3</option>
          <option value="4">Driver 4</option>
          </select>
          <label class="block-label" for="condition">Condition</label>
          <select class="block-input">
          <option value="sunny">Sunshine</option>
          <option value="overcast">Overcast</option>
          <option value="drizzle">Drizzle</option>
          <option value="rain">Heavy Rain</option>
          <option value="wind">Windy</option>
          </select>
          <label class="block-label" for="temp">Track Temperature (celsius)</label>
          <input class="block-input" type="text" name="temp" placeholder="15 degrees" required />
          <input type="submit" value="Upload" class="block-button"/>
        </form>
        </div>
        
        <div class="trim">
        <h1>TRIM</h1>
        <img class="block-label" src="img/graph.jpg" alt="graph" height="250" width=100% style="border: 1px solid black;">
        <label class="block-label">RUN TIME XX:XX</label>
        <label class="block-label" for="driver">Y Value</label>
          <select class="block-input">
          <option value="1">Y value 1</option>
          <option value="2">Y value 2</option>
          <option value="3">Y value 3</option>
          <option value="4">Y value 4</option>
          </select>
        <label class="block-label" for="new-start-time">New Start Time</label>
          <input class="block-input" type="text" name="new-start-time" placeholder="New Start Time" required />
          <label class="block-label" for="new-end-time">New End Time</label>
          <input class="block-input" type="text" name="new-end-time" placeholder="New End Time" required />
          <input type="submit" value="Trim" class="block-button"/>
          <input type="submit" value="Export" class="block-button"/>
        </div>
        
        </section>
</body>
</html>

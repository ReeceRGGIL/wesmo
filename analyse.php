<?php 
  session_start();
  require("/include/logged-in.inc.php");
?>
<!DOCTYPE html>
<head>
  <title>Analyse - Wesmo Viewer</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="icon" href="img/wesmo-icon.ico">
  
  <link href="css/analyse-demo-style.css" rel="stylesheet" type="text/css">
  <link href="vendor/nvd3/nv.d3.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="vendor/d3js/d3.v3.min.js"></script>
  <script type="text/javascript" src="js/analyse-demo-script.js"></script>
  <script type="text/javascript" src="vendor/nvd3/nv.d3.js"></script>
</head>
<body onload="createGraph()">
  
  <?php include('include/navbar.inc.php') ?>
  
  <!--<nav class="nav-bar">
    <a href="analyse.php">
      <div class="nav-div">Analyse</div>
    </a>
    <a href="data.php">
      <div class="nav-div">Data</div>
    </a>
    <a href="members.php">
      <div class="nav-div">Members</div>
    </a>
    <a href="php/logout.php">
      <div class="nav-div nav-div-right">Log out</div>
    </a>
    <div class="nav-div nav-div-right">
      <div class="vertical-content">
        <img class="nav-logo" src="img/wesmo-logo-black.png" alt="WESMO Logo">
      </div>
    </div>
  </nav>-->
  
  <!-- Container for all main content on the page -->
  <div class="content">
    <div id="mainContent">
      
          
      <div id="body">
        <div id="chart">
          <svg></svg>
        </div>
        <div id="selection">
          <div id="availableRuns">
            <h4>Available runs:</h4>
            <select id="available" size="9"></select>
            <button onclick="addSelectedRuns()">Select</button>
            <button onclick="search()" id="searchButton">Search</button>
          </div>
          <div id="selectedRuns">
            <h4>Selected runs:</h4>
            <select id="selected" size="5"></select>
            <button onclick="removeSelectedRuns()">Remove</button>
          </div>
        </div>
        <div id="attributes">
          <div id="xcontainer">
            <b>X-axis:</b>
            <select id="xaxis" onchange="selectXAxis(this.value)">
            </select>
          </div>
          <div id="ycontainer">
              <b>Y-axis:</b>
            <select id="yaxis" onchange="selectYAxis(this.value)">
            </select>
          </div>
        </div>
        <div id="statistics">
          <h4>Statistics:</h4>
          <div class="datagrid">
              <table>
                  <thead>
                      <tr id="header"><th></th><th>Runs</th></tr>
                  </thead>
                  <tbody id="data">
                      <tr><td class="rowName">Conditions</td><td></td></tr>
                      <tr class="alt"><td class="rowName">Track temp.</td><td></td></tr>
                      <tr><td class="rowName">Avg</td><td></td></tr>
                      <tr class="alt"><td class="rowName">Min</td><td></td></tr>
                      <tr><td class="rowName">Max</td><td></td></tr>
                  </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>

</body>

<!DOCTYPE html>
<html>
  <head>
    <title>Unapproved - Wesmo Viewer</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="icon" href="img/wesmo-icon.ico">
  </head>
  <body>
    <div class="vertical-content">
      <div class="login">
        <img src="img/wesmo-logo-black.png" alt="wesmo logo">
        <h1>Waiting For Approval</h1>
        <p>You account has been registered and is currently awaiting approval from a manager. You will be email once you account has been approved.</p>
      </div>
    </div>
  </body>
</html>
